![Core2Web Logo](https://github.com/ganeshbagav7/ccpl/assets/72212389/39760577-99a2-4c5b-abab-f016a0f95ffd)

Welcome to the Core2Web Programming Library, a C language project that empowers you to execute, print output, and download C language code files with a user-friendly GUI using Gtk and Glade. This project is designed to make programming in C more enjoyable and accessible for everyone, whether you are a beginner or an experienced developer.

## Demonstration Video

[![IMAGE ALT TEXT HERE](https://github.com/ganeshbagav7/ccpl/assets/72212389/dcfba6f5-eaf5-4794-a4e4-05208a05ce51)](https://www.youtube.com/watch?v=ZrevNMaIaW4)


## Technologies Used 🛠️

- Glade
- Gtk
- C Language
- Git

## Sections and Features 📚

### Concepts 🧠

In this section, you can delve into the fundamental concepts of the C language. We provide concise explanations and examples to help you grasp the core concepts effectively.

### Array 📊

The Array section includes various C language codes for manipulating arrays, such as reversing arrays and sorting arrays. You'll find efficient algorithms and illustrative examples.

### String 🔤

Operations on strings are made simple in this section. Whether you want to find the first repeated character in a string or determine if a string is palindromic, we've got you covered.

### Pattern 🎨

Get creative with this section, where you can print various patterns using asterisks and other symbols. Unleash your artistic side and explore different patterns.

```
* * *
* * *
* * *
```

### Numbers 🔢

Working with numbers becomes a breeze with the Numbers section. You can determine if a number is palindromic, even, odd, or an Armstrong number effortlessly.

### Data Structure 📚

In this exciting section, we bring data structures to life with a user-friendly GUI. You can experiment with linked lists, stacks, and queues, all within the comfort of the application.

## Setup environment to compile and run the C program with GTK and Glade 

Follow the steps listed in 'Gtk and Glade setup.pdf' 

## How to Use 📖

1. Clone the repository using the following command and navigate to the ccpl folder:
   ```
   git clone https://gitlab.com/project868/codeexplorer
   cd ccpl
   ```

2. Compile the C code using a C compiler (e.g., gcc) and run the executable, to make it easier we already have an executable bash script in EXECUTABLE.txt, just run the script using the following command and run the 'Core2Web':
   ```
   bash EXECUTABLE.txt
   ./Core2Web
   ```

3. The GUI interface will open up, presenting you with the sections and functionalities of the Core2Web Programming Library. Simply click on any section to explore the concepts or execute the desired C code.
